package com.quinielas.util;

import java.util.Hashtable;
import java.util.ArrayList;
import java.io.IOException;

// gestiona todo lo referido a la comunicacion con el usuario
public class Pantalla{

    public void cabecera(){
		System.out.println("=========================================");
		System.out.println("= ASISTENTE DE PLAYMANAGER - BALONCESTO =");
		System.out.println("=========================================");
		System.out.println();        
    }
    
    public String pedirOpcion() throws IOException{
        int c;
        System.out.println("Opción [0..9] (1=Ayuda): ");
        do {
            c = System.in.read();            
        } while (c < 48 || c > 57);
        return String.valueOf((char)c);
    }
    
    public void ayuda(){
        System.out.println("0 = Salir");    
        System.out.println("1 = Ayuda");        
        System.out.println("2 = Cargar plantilla desde fichero");        
        System.out.println("3 = Mostrar plantilla");
        System.out.println("4 = Sin definir");
        System.out.println("5 = Sin definir");
        System.out.println("6 = Sin definir");
        System.out.println("7 = Sin definir");
        System.out.println("8 = Sin definir");
        System.out.println("9 = Status");        
    }
    
    public void mostrarTexto(String texto){
        System.out.println(texto);    
    }        
    
    public void mostrarTabla(ArrayList<Hashtable<String, String>> datos, String[] cabecera){
        
        Tabla tabla = new Tabla();
        tabla.setTitulo("PLANTILLA");
        tabla.setCabecera(cabecera);
        tabla.setAnchoCaracteres(75);
        String[] contenido = tabla.mostrarTabla(datos, cabecera);
        
        for (String fila : contenido) System.out.println(fila);

    }
        
}