package com.quinielas.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.LinkedList;

public class Migrador {
	
	
	// posicion en array de partido
    final byte TEMPORADA		= 0;
    final byte DIVISION			= 1;
    final byte JORNADA			= 2;
    final byte EQUIPO_LOCAL		= 3; 
    final byte EQUIPO_VISITANTE	= 4;
    final byte GOLES_LOCAL		= 5;
    final byte GOLES_VISITANTE	= 6;
    final byte TIPO_PARTIDO		= 7;
    
    
    // contadores de elementos
    // para calcular ID.
    private int cont_edicion_1 	= 0;
    private int cont_edicion_2	= 0;
    private int cont_equipos	= 0;

    private int cont_partidos		= 0;
	
    // Colecciones de tipo Linked para mantener la ordenación.
    // Imprescindible, a falta de fechas, para calcular valoraciones de partidos.
	private LinkedHashMap<String, Integer> ediciones = new LinkedHashMap<String, Integer>();
	private LinkedHashMap<String, Integer> equipos = new LinkedHashMap<String, Integer>();
	private LinkedHashMap<String[], Integer> partidos = new LinkedHashMap<String[], Integer>();
	private LinkedList<String> participantes = new LinkedList<String>();

	public void migrar(String fichero_origen, String fichero_destino){
		leer_fichero(fichero_origen);
		generar_inserts(fichero_destino);
		
		/*
        Enumeration enumeration = ediciones.keys();
        Object elemento;
        Object contador;
        
        while ( enumeration.hasMoreElements() ){
        	  elemento = enumeration.nextElement();
        	  contador = ediciones.get( elemento );
        	  
        	  System.out.println(elemento + " - " + contador);
  			
        }
        */				
	}
	
	private void leer_fichero(String fichero) {
		// TODO Auto-generated method stub
		//File fichero = new File("../data/historico_liga.txt");
		//fichero = "/home/juanluis/git/guru1x2/data/historico_liga.txt";
		//String fichero = "/home/juanluis/git/guru1x2/data/prueba.txt";
	    try {
	        FileInputStream fis = new FileInputStream(fichero);
	        InputStreamReader isr = new InputStreamReader(fis,"cp1252");
	        //InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
	        BufferedReader br = new BufferedReader(isr);
	   
	        String linea = null;
    
	        while(br.ready()){
	        	linea = br.readLine();
	        	
	        	String[] datos_partido = new String[8];
	        	
	        	datos_partido[DIVISION]	= extraer(linea, "DIVISION");
	        	// controlar partidos de promoción
	        	if ("1".equals(datos_partido[DIVISION]) || "2".equals(datos_partido[DIVISION])){	        	
	        		datos_partido[TIPO_PARTIDO] = "1";
	        		datos_partido[JORNADA]	= extraer(linea, "JORNADA");
	        	} else {
	        		// partidos de promoción/descenso se consideran de 1ª división
	        		// los partidos de promoción no figuran con jornada
	        		datos_partido[DIVISION] = "1";
	        		datos_partido[TIPO_PARTIDO] = "2";
	        		datos_partido[JORNADA]	= "0";

	        	}
	        	datos_partido[TEMPORADA] = codificar_edicion(datos_partido[DIVISION] + "-" + 
						extraer(linea, "TEMPORADA"));
	        	datos_partido[EQUIPO_LOCAL]	= codificar_equipo(extraer(linea, "EQUIPO_LOCAL"));
	        	registrar_participante( datos_partido[DIVISION] + ":" + 
	        							datos_partido[TEMPORADA] + ":" + 
	        							datos_partido[EQUIPO_LOCAL]);
	        	datos_partido[EQUIPO_VISITANTE]	= codificar_equipo(extraer(linea, "EQUIPO_VISITANTE"));
	        	registrar_participante(	datos_partido[DIVISION] + ":" + 
	        							datos_partido[TEMPORADA] + ":" + 
	        							datos_partido[EQUIPO_VISITANTE]);
	        	datos_partido[GOLES_LOCAL]	= extraer(linea, "GOLES_LOCAL");
	        	datos_partido[GOLES_VISITANTE]	= extraer(linea, "GOLES_VISITANTE");
	        	this.cont_partidos++;
	        	this.partidos.put(datos_partido, this.cont_partidos);
	        	
	        	
	        	/*
	        	System.out.println("TEMPORADA: " + extraer(linea, "TEMPORADA"));
	        	System.out.println("DIVISION: " + extraer(linea, "DIVISION"));
	        	System.out.println("JORNADA: " + extraer(linea, "JORNADA"));
	        	System.out.println("LOCAL: " + extraer(linea, "EQUIPO_LOCAL"));
	        	System.out.println("VISITANTE: " + extraer(linea, "EQUIPO_VISITANTE"));
	        	System.out.println("GOLES LOCAL: " + extraer(linea, "GOLES_LOCAL"));
	        	System.out.println("GOLES_VISITANTE: " + extraer(linea, "GOLES_VISITANTE"));
	        	*/
	        }
	        fis.close();
	        //mostrar_lista(this.equipos);
	        
	      } catch(Exception e) {
	        System.out.println("Excepcion leyendo fichero "+ fichero + ": " + e);
	      }
	}
	
	private void generar_inserts(String fichero){
		
        FileWriter fw = null;
        PrintWriter pw = null;
        try
        {
            fw = new FileWriter(fichero);
            pw = new PrintWriter(fw);
            
            String linea = null;
            
            // iniciar transacción
            //pw.println("BEGIN TRANSACTION");
            
            // INSERTS TABLAS BASES
            pw.println("INSERT INTO TIPOS_COMPETICION(tc_id, tc_denominacion) VALUES (1, 'Liga Nacional');");
            pw.println("INSERT INTO TIPOS_EQUIPO(te_id, te_denominacion) VALUES (1, 'Club');");
            pw.println("INSERT INTO TIPOS_PARTIDO(tp_id, tp_denominacion) VALUES (1, 'Jornada liga');");
            pw.println("INSERT INTO TIPOS_PARTIDO(tp_id, tp_denominacion) VALUES (2, 'Promoción ascenso/descenso');");
            pw.println("INSERT INTO COMPETICIONES(comp_id, comp_nombre, comp_tipo) VALUES (1, 'Liga Española 1ª DIVISIÓN', 1);");
            pw.println("INSERT INTO COMPETICIONES(comp_id, comp_nombre, comp_tipo) VALUES (2, 'Liga Española 2ª DIVISIÓN', 1);");
            
            //INSERT INTO <tabla>(<campo1>, <campo2>) VALUES (<valor1>, <valor2>);
            
            // objetos para recorrer LinkedHashMaps
            Iterator<String> iterador; 
            String elemento;
            Integer contador;            
            
            // ediciones
            final byte ED_COMP		= 0;
            final byte ED_INICIO 	= 1;
            final byte ED_FIN		= 2;
            
    		iterador = ediciones.keySet().iterator();
    		while(iterador.hasNext()){
    			  elemento = iterador.next();
    			  contador = ediciones.get(elemento);
    			  String[] edicion = elemento.split("-");
    			  linea = String.format("INSERT INTO EDICIONES(ed_comp_id, ed_id, ed_denominacion, ed_fecha_inicio, ed_fecha_fin) VALUES (%s, %s, '%s', '%s', '%s');", edicion[ED_COMP], contador , edicion[ED_INICIO] + "/" + edicion[ED_FIN], edicion[ED_INICIO] + "-09-01", edicion[ED_FIN] + "-06-01") ;
    			  pw.println(linea);    			  
    		}		

    		// equipos
    		iterador = equipos.keySet().iterator();
            while ( iterador.hasNext() ){
            	elemento = iterador.next();
            	contador = equipos.get(elemento);
            	linea = String.format("INSERT INTO EQUIPOS(eq_id, eq_nombre, eq_tipo) VALUES(%s, '%s', 1);", contador, elemento);
            	pw.println(linea);
            }
            
    		// participantes
    		final byte PAR_DIVISION 	= 0;
    		final byte PAR_TEMPORADA 	= 1;
    		final byte PAR_EQUIPO		= 2;
    		
    		iterador = participantes.iterator();
    		
    		while ( iterador.hasNext() ){
    			elemento = iterador.next();
    			String[] participante = elemento.split(":");
    			linea = String.format("INSERT INTO PARTICIPANTES(comp_id, edicion, equipo_id) VALUES(%s, %s, %s);", participante[PAR_DIVISION], participante[PAR_TEMPORADA], participante[PAR_EQUIPO]);
    			pw.println(linea);   			
    		}
    		
    		// partidos
    		Iterator<String[]> iterador_partidos;
    		String[] partido;
    		iterador_partidos = partidos.keySet().iterator();
    		
    		while ( iterador_partidos.hasNext() ){
    			partido = iterador_partidos.next();
    			contador = partidos.get(partido);
    			linea = String.format("INSERT INTO PARTIDOS(partido_id, comp_id, ed_id, partido_tipo, partido_num, equipo_local, equipo_visitante, goles_local, goles_visitante) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s);", contador, partido[DIVISION], partido[TEMPORADA], partido[TIPO_PARTIDO], partido[JORNADA], partido[EQUIPO_LOCAL], partido[EQUIPO_VISITANTE], partido[GOLES_LOCAL], partido[GOLES_VISITANTE]);
    			pw.println(linea);
    		}
    		
    		// confirmar transacción
    		//pw.println("COMMIT");
    		            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           // Nuevamente aprovechamos el finally para 
           // asegurarnos que se cierra el fichero.
           if (null != fw)
              fw.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }		
		
	}
	
	private String extraer(String cadena, String elemento){
		// extrae los elementos de la cadena.
		// pensado para las líneas de fichero de texto fuente
		
		int inicio = 0;
		int fin = 0;
	    // indices a posición de caracteres en fichero de txt
		
		switch (elemento){
			case "TEMPORADA":
				inicio 	= 0;
				fin		= 9;
				break;
			case "DIVISION":
				inicio	= 13;
				fin		= 14;
				break;
			case "JORNADA":
				inicio	= 21;
				fin		= 23;
				break;
			case "EQUIPO_LOCAL":
				inicio	= 28;
				fin		= 45;
				break;
			case "EQUIPO_VISITANTE":
				inicio	= 45;
				fin		= 61;
				break;
			case "GOLES_LOCAL":
				inicio	= 61;
				fin		= 63;
				break;
			case "GOLES_VISITANTE":
				inicio	= 64;
				fin		= cadena.length();
				break;				
		}
		
		return cadena.substring(inicio, fin).trim();
	}
	
	private void registrar_participante(String participante){
		
		if (!this.participantes.contains(participante)) {
			this.participantes.add(participante);
		}
	}
	
	private String codificar_edicion(String edicion){
		int id = 0;
		if ( this.ediciones.containsKey(edicion) ){
			id = this.ediciones.get(edicion);
		} else {
			if (edicion.charAt(0) == '1' ){
				this.cont_edicion_1 += 1; 
				id = this.cont_edicion_1;
			} else if ( edicion.charAt(0) == '2' ){
				this.cont_edicion_2 += 1;
				id = this.cont_edicion_2;
			}
			this.ediciones.put(edicion, id);
		}
		return Integer.toString(id);
	}
	
	private String codificar_equipo(String equipo){
		int id = 0;
		if ( this.equipos.containsKey(equipo) ){
			id = this.equipos.get(equipo);
		} else {
			this.cont_equipos += 1;
			id = this.cont_equipos;
			this.equipos.put(equipo, id);
		}
		return Integer.toString(id);
	}

	private void mostrar_lista(LinkedHashMap lista){
		
		Iterator<String> it = lista.keySet().iterator();
		
		while(it.hasNext()){
			  String clave = it.next();
			  System.out.println("Clave: " + clave + " -> Valor: " + lista.get(clave));
			}		

	}
	
	
}
