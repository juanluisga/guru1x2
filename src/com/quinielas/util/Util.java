package com.quinielas.util;

public class Util{
    
    public static String concatenar(String cadena, String subcadena){
        cadena = cadena.concat(subcadena);
        return cadena;
    }
    
    public static String centrarCadena(String cadena, int espacio){
        String cadenaCentrada = "";
        int puntero = (espacio + 1 - cadena.length()) / 2;
        for (int i = 1; puntero > 0 && i < puntero; i++) 
            cadenaCentrada = cadenaCentrada.concat(" ");
        cadenaCentrada = cadenaCentrada.concat(cadena);
        for (int i = puntero - 1 + cadena.length(); puntero > 0 && i < espacio; i++) 
            cadenaCentrada = cadenaCentrada.concat(" ");
        return cadenaCentrada;        
    }
    
    public static String espaciosDerecha(String cadena, int anchura_total) {
        return String.format("%1$-" + anchura_total + "s", cadena);  
    }

    public static String espaciosIzquierda(String cadena, int anchura_total) {
        return String.format("%1$" + anchura_total + "s", cadena);  
    } 
    
    public static String justificarIzquierda(String cadena, int espacio){
        //return String.format( "%1$-" + Integer.toString( espacio - cadena.length() ) + "s", cadena );
        while (cadena.length() < espacio) cadena = cadena.concat(" ");
        return cadena; 
    }   
    
    public static String justificarDerecha(String cadena, int espacio){
        //return String.format( "%1$" + Integer.toString( espacio - cadena.length() ) + "s", cadena );
        while (cadena.length() < espacio) cadena = " ".concat(cadena);
        return cadena;
    }
}