package com.quinielas.util;

import java.io.*;
import java.util.ArrayList;

public class FicheroCSV {
	
	private String 		fichero;
	private String 		codificacion 	= "UTF8";
	private String 		separador 		= ";";
	private boolean 	flagCabecera 	= false;
	private String[]	cabecera;

	
	// Constructores
	public FicheroCSV(){
		;
	}
	
	public FicheroCSV(String fichero, String codificacion, String separador, boolean flagCabecera){
			this.fichero 		= fichero;
			this.codificacion	= codificacion;
			this.separador		= separador;
			this.flagCabecera	= flagCabecera;
			
	}
	
	//para ficheros con símbolos propios del español, 
    //utilizar la codificación "ISO-8859-1"
	public void setCodificacion(String codificacion){
		this.codificacion = codificacion;
	}
	
	public String getCodificacion(){
		return this.codificacion;
	}
	
	public void setFichero(String fichero){
		this.fichero = fichero;
	}
	
	public String getFichero(){
		return this.fichero;
	}
	
	public void setSeparador(String separador){
		this.separador = separador;
	}
	
	public String getSeparador(){
		return this.separador;
	}
	
	public void setFlagCabecera(boolean flagCabecera){
		this.flagCabecera = flagCabecera;
	}
	
	public boolean getFlagCabecera(){
		return this.flagCabecera;
	}
	
	public void setCabecera(String[] cabecera){
		this.cabecera = cabecera;
		this.flagCabecera = true;
	}
	
	public String[] getCabecera(){
		return this.cabecera;
	}
	
	public ArrayList<String[]> Leer() 
		    throws UnsupportedEncodingException, FileNotFoundException, IOException{
			
		ArrayList<String[]> datos=new ArrayList<String[]>();
	    FileInputStream fis =new FileInputStream(this.fichero);
	    InputStreamReader isr = new InputStreamReader(fis, this.codificacion); 
	    BufferedReader br = new BufferedReader(isr);

        String linea=br.readLine();
		while(linea!=null){
		  datos.add(linea.split(this.separador));
		  linea=br.readLine();
		}

		br.close();
		isr.close();
		fis.close();
		    
		// Si el fichero csv contiene cabecera, esta se elimina.
		if (this.flagCabecera) datos.remove(0);
		    
		return datos;

	}

	public void Escribir(ArrayList<String[]> d)
	   throws UnsupportedEncodingException, FileNotFoundException, IOException{

	    OutputStream fout = new FileOutputStream(this.fichero);
	    OutputStreamWriter out = new OutputStreamWriter(fout, this.codificacion);
		    
	    /* Si tiene cabecera se la añadimos al principio del ArrayList */
	    if (this.flagCabecera) d.add(0, this.cabecera);
    	    for(int i=0; i<d.size(); i++){

		        String[] fila=d.get(i);

		        for (int j=0; j<fila.length; j++){
		            out.write(fila[j]+this.separador);
		        }

		        out.write("\n");
		    }
		    out.close();
		    fout.close();
		}
}
