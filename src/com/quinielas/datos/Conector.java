package com.quinielas.datos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;
import java.util.logging.Level;

public class Conector {
	String url;
	Connection conexion;
	
	public Conector(){
		
	}
	public Conector(String url){
		this.url = url;
	}
	
	public void conectar(){
		try {
			this.conexion = DriverManager.getConnection("jdbc:sqlite:" + this.url);
		    if (this.conexion != null) {
		    	System.out.println("Conectado");
		    }
		}catch (SQLException ex) {
			System.err.println("No se ha podido conectar a la base de datos\n" + ex.getMessage());
		}
	}
	
	public void cerrar(){
		try {
			conexion.close();
		} catch (SQLException ex) {
		    Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
		}
	}	
}
