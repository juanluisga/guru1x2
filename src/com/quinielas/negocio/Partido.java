/**
 * 
 */
package com.quinielas.negocio;

import com.quinielas.datos.Conector;

/**
 * @author juanluis
 *
 */
public class Partido extends Conector{
	
	public Partido(){
		super();
	}
	
	public Partido(String url){
		super(url);
	}
	
	public String[] pronostico(){
		String[] pronostico = new String[2];
		pronostico[0] 		= tanteo_equipo(true);
		pronostico[1] 		= tanteo_equipo(false);
		return pronostico;
		
	}
	
	private String tanteo_equipo(boolean local){
		// calcula un pronóstico de tanteo para un equipo
		// dependiendo si es local o visitante.
		
		float peso_0;
		float peso_1;
		float peso_2;
		//float peso_M;
		
		if (local){
			peso_0 = 0.2f;
			peso_1 = 0.3f;
			peso_2 = 0.3f;
			//peso_M = 0.2f;			
		} else {
			peso_0 = 0.4f;
			peso_1 = 0.3f;
			peso_2 = 0.2f;
			//peso_M = 0.1f;				
		}
		
		double valor = Math.random();
		String tanteo;
		if (valor <= peso_0){
			tanteo = "0";
		} else
			if (valor <= peso_0 + peso_1){
				tanteo = "1";
			} else
				if (valor <= peso_0 + peso_1 + peso_2){
					tanteo = "2";
				} else
					tanteo = "M";
		return tanteo;
	}
}
