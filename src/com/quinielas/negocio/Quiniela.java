package com.quinielas.negocio;

public class Quiniela {
	
	String[][] apuestas;
	String[] pleno_15;
	
	// definir probabilidades
	final static float PESO_1 = 0.6f;
	final static float PESO_X = 0.3f;
	//final static float PESO_2 = 0.1f;

	public Quiniela(){
		// vacio
	}
	
	public Quiniela(int numApuestas){
		this.generar(numApuestas);
	}
	
	public int getNumApuestas(){
		return apuestas.length;
	}
	
	public String[][] getBloqueApuestas(){
		return apuestas;
	}
	
	public String[] getPleno_15(){
		return pleno_15;
	}
	
	public void generar(int numApuestas){
		
		// dimensionar apuestas
		this.apuestas = new String[numApuestas][14];

		// generar pronósticos
		numApuestas--;
		while (numApuestas >= 0){
			for (byte pronostico = 0; pronostico <= 13; pronostico++){
				double valor = Math.random();
				String signo;
				if (valor <= PESO_1){
					signo = "1";
				} else
					if (valor <= PESO_1 + PESO_X){
						signo = "X";
					} else
						signo = "2";
				apuestas[numApuestas][pronostico] = signo;
			}
			
			numApuestas--;
		}
		
		// pleno al 15
		Partido partido = new Partido();
		pleno_15 = partido.pronostico(); 

		
	}

}
