package com.quinielas.presentacion.gui;

//import java.awt.EventQueue;



import com.quinielas.negocio.Quiniela;
import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.event.*;
import java.awt.*;


public class Guru_gui extends JFrame implements ActionListener {
	
	private JPanel panNumApuestas;
	private JLabel lblNumApuestas;
	private JTextField txtNumApuestas;;
	private JButton btnNumApuestas;	
	private JButton btnLimpiar;
	private GridQuiniela grid;
	private Quiniela quiniela;
	
	public Guru_gui(){
		super("guru1x2");
		setSize(400, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		
		panNumApuestas = new JPanel(new FlowLayout());
		panNumApuestas.setBorder(new BevelBorder(BevelBorder.RAISED));
		lblNumApuestas = new JLabel("Nº de apuestas");
		panNumApuestas.add(lblNumApuestas);
		
		txtNumApuestas = new JTextField("2", 2);
		txtNumApuestas.setHorizontalAlignment(JTextField.RIGHT);
		panNumApuestas.add(txtNumApuestas);
		
		btnNumApuestas = new JButton("Generar");
		panNumApuestas.add(btnNumApuestas);
		btnNumApuestas.addActionListener(this);
		
		btnLimpiar = new JButton("Limpiar");
		panNumApuestas.add(btnLimpiar);
		btnLimpiar.addActionListener(this);
		
		
		add(panNumApuestas);
		
		grid = new GridQuiniela();
		add(grid);
		
		setVisible(true);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent evento){
		if (evento.getSource() == this.btnNumApuestas ){
			this.generarQuiniela(Integer.parseInt(this.txtNumApuestas.getText()));
		}
		if (evento.getSource() == this.btnLimpiar ){
			grid.limpiar();
		}
	}
	
	
	private void generarQuiniela(int numApuestas){
		//btnGenerar.setVisible(false);
		this.quiniela = new Quiniela(numApuestas);
		grid.mostrar(this.quiniela);
	}
}

