package com.quinielas.presentacion.gui;

import com.quinielas.negocio.Quiniela;
import javax.swing.table.DefaultTableModel;
import javax.swing.*;
import java.awt.*;
import java.util.*;

public class GridQuiniela extends JPanel {

	private JTable gridQuinela;
	private DefaultTableModel modeloTabla;
	private JScrollPane scroll;
	private JLabel pleno_15;
		
	
	public GridQuiniela(){
		super();
		construir();
	}
	
	public GridQuiniela(Quiniela quiniela){
		super();
		construir();
		mostrar(quiniela);
	}
	
	private void construir(){
		this.setLayout( new BoxLayout(this,BoxLayout.Y_AXIS));
		this.modeloTabla = new DefaultTableModel();
		String[] partidos = {	"Partido 1", 
								"Partido 2", 
								"Partido 3", 
								"Partido 4", 
								"Partido 5",
								"Partido 6",
								"Partido 7",
								"Partido 8",
								"Partido 9",
								"Partido 10",
								"Partido 11",
								"Partido 12",
								"Partido 13",
								"Partido 14"};
		this.modeloTabla.addColumn("PARTIDOS", partidos);
		gridQuinela = new JTable(modeloTabla);
		gridQuinela.setPreferredScrollableViewportSize(new Dimension(350,240));
		this.scroll = new JScrollPane(gridQuinela);
		add(this.scroll, BorderLayout.CENTER);
		
		pleno_15 = new JLabel("Pleno al 15: ");
		add(pleno_15);
		
	}
	
	public void mostrar(Quiniela quiniela){
		
		for ( byte i = 0; i < quiniela.getNumApuestas(); i++ ){
			this.modeloTabla.addColumn("columna ", quiniela.getBloqueApuestas()[i]);
		}
		this.repaint();
	}
	
	public void limpiar(){
		for (int i = 1; i < this.modeloTabla.getColumnCount(); i++){
			
		}
	}
	
	

}
