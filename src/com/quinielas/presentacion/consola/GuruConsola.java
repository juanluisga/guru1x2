package com.quinielas.presentacion.consola;

import java.util.Scanner;
import com.quinielas.negocio.Quiniela;
import com.quinielas.util.Util;

public class GuruConsola extends Consola{

	/*
	public GuruConsola(){
		super();
		iniciar();
	}
	 */	
	
	public GuruConsola(String titulo){
		super(titulo);
		iniciar();
	}

	public void iniciar(){
		super.iniciar();
		// Aquí debería invocarse a menú
		generarQuiniela();
	}
	
	private void generarQuiniela(){
		Quiniela quiniela = new Quiniela();
		quiniela.generar(pedirNumeroApuestas());
		
		// mostrar boleto
		Integer num_partido;
		String cadena;
		for (int pronostico = 0; pronostico <= 13; pronostico++){
			num_partido = pronostico + 1;
			cadena = Util.espaciosDerecha("Partido " + num_partido.toString(), 10) + " ";
			for (int apuesta = 0; apuesta < quiniela.getNumApuestas(); apuesta++){
				cadena = cadena + quiniela.getBloqueApuestas()[apuesta][pronostico] + " ";
			
			}
			System.out.println(cadena);
			cadena = null;
		}
		System.out.println("PLENO AL 15: local " + quiniela.getPleno_15()[0] + "  visitante " + quiniela.getPleno_15()[1]);
	}
	
	private int pedirNumeroApuestas(){
		Scanner scanner = new Scanner(System.in);
		System.out.println("Número de apuestas: ");
		int num_apuestas = scanner.nextInt();
		scanner.close();
		return num_apuestas;
	}
}
