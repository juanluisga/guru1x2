package com.quinielas.presentacion.consola;


public class Consola {
	
	private String titulo = null;
	
	public Consola(){
		
	}
	
	public Consola(String titulo){
		setTitulo(titulo);
	}
	
	public void setTitulo(String titulo){
		this.titulo = titulo;
	}
	
	public String getTitulo(){
		return this.titulo;
	}
	
	public void iniciar(){
		this.cabecera(titulo);
	}
	
	private void cabecera(String titulo){
		System.out.println(titulo.replaceAll("(?s).", "*") + "**");
		System.out.println("*" + titulo + "*");
		System.out.println(titulo.replaceAll("(?s).", "*") + "**");
	}

}
