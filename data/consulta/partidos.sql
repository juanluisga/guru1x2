SELECT ED.ed_denominacion, COM.comp_nombre, PAR.partido_num, LOCAL.eq_nombre, VISITANTE.eq_nombre, PAR.goles_local, PAR.goles_visitante
	FROM PARTIDOS as PAR
		LEFT JOIN EDICIONES as ED ON (PAR.comp_id = ED.ed_comp_id AND PAR.ed_id = ED.ed_id )
		LEFT JOIN COMPETICIONES as COM ON (PAR.comp_id = COM.comp_id)
		LEFT JOIN EQUIPOS as LOCAL ON (PAR.equipo_local = LOCAL.eq_id)
		LEFT JOIN EQUIPOS as VISITANTE ON (PAR.equipo_visitante = VISITANTE.eq_id);