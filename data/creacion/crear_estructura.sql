
	CREATE TABLE TIPOS_PARTIDO (
		tp_id			INTEGER NOT NULL PRIMARY KEY,
		tp_denominacion	INTEGER NOT NULL
	);
	
	CREATE TABLE TIPOS_EQUIPO (
		te_id			INTEGER NOT NULL PRIMARY KEY,
		te_denominacion	TEXT NOT NULL
	);
	
	CREATE TABLE EQUIPOS (
		eq_id		INTEGER NOT NULL PRIMARY KEY,
		eq_nombre	TEXT NOT NULL,
		eq_tipo		INTEGER,
		FOREIGN KEY(eq_tipo) REFERENCES TIPOS_EQUIPO(te_id)
	);
	
	CREATE TABLE TIPOS_COMPETICION (
		tc_id			INTEGER NOT NULL PRIMARY KEY,
		tc_denominacion	TEXT NOT NULL
	);
	
	CREATE TABLE COMPETICIONES (
		comp_id		INTEGER PRIMARY KEY,
		comp_nombre	TEXT NOT NULL,
		comp_tipo	INTEGER,
		FOREIGN KEY(comp_tipo) REFERENCES TIPOS_COMPETICION(tc_id)
	);
	
	CREATE TABLE EDICIONES (
		ed_comp_id		INTEGER NOT NULL,
		ed_id			INTEGER NOT NULL,
		ed_denominacion	TEXT DEFAULT NULL,
		ed_sede			TEXT DEFAULT NULL,
		ed_fecha_inicio	TEXT DEFAULT NULL,
		ed_fecha_fin	TEXT DEFAULT NULL,
		PRIMARY KEY(ed_comp_id,ed_id),
		FOREIGN KEY(ed_comp_id) REFERENCES COMPETICIONES(comp_id)
	);

	CREATE TABLE PARTICIPANTES (
		comp_id		INTEGER NOT NULL,
		edicion		INTEGER NOT NULL,
		equipo_id	INTEGER NOT NULL,
		PRIMARY KEY(comp_id,edicion,equipo_id),
		FOREIGN KEY(comp_id, edicion) REFERENCES EDICIONES(ed_comp_id, ed_id)
	);
	
	CREATE TABLE PARTIDOS (
		partido_id			INTEGER NOT NULL PRIMARY KEY,
		comp_id				INTEGER NOT NULL,
		ed_id				INTEGER NOT NULL,
		partido_tipo		INTEGER,
		partido_num			INTEGER DEFAULT NULL,
		fecha				TEXT DEFAULT NULL,
		equipo_local		INTEGER NOT NULL,
		equipo_visitante	INTEGER NOT NULL,
		goles_local			INTEGER NOT NULL,
		goles_visitante		INTEGER NOT NULL,
		FOREIGN KEY(comp_id, ed_id) REFERENCES EDICIONES(ed_comp_id, ed_id),
		FOREIGN KEY(partido_tipo) REFERENCES TIPOS_PARTIDO(tp_id),
		FOREIGN KEY(comp_id, ed_id, equipo_local) REFERENCES PARTICIPANTES(comp_id, edicion, equipo_id),
		FOREIGN KEY(comp_id, ed_id, equipo_visitante) REFERENCES PARTICIPANTES(comp_id, edicion, equipo_id)
	);
	
