#! /usr/bin/env python3
try:
    with open("../data/equipos_filtrado.txt", "r", encoding="cp1252") as archivo_equipos, open("../data/inserts_equipos.sql", "w", encoding="cp1252") as archivo_inserts:
        for equipo in archivo_equipos:
            insert = "INSERT INTO teams(team_name) VALUES ('%s');\n" % equipo.strip()
            archivo_inserts.write(insert)
except IOError as e:
    print('Operation failed: %s' % e.strerror)
        