#! /usr/bin/env python3
# -*- coding: cp1252 -*-
 
def codificar_edicion(edicion, dic_ediciones):
    if not edicion in dic.ediciones.keys():
        dic_ediciones[edicion] = len(dic_ediciones) + 1
    return dic_ediciones[edicion]
 
def codificar_equipo(equipo, dic_equipos):
    # Al ser los diccionarios un tipo mutable,
    # el del parametro lista_equipo debe ser POR REFERENCIA.
    
    #if equipo == "Almer�a (A.D.)":
    #    equipo = "Almer�a"
    #elif equipo == "Burgos (Real)":
    #    equipo = "Burgos" 
    #elif equipo == "Cartagena F.C.": 
    #    equipo = "Cartagena"
    #elif equipo == "M�laga (C.D.)":
    #    equipo = "M�laga"
    if not equipo in dic_equipos.keys():
        dic_equipos[equipo] = len(dic_equipos) + 1
    return dic_equipos[equipo]

ediciones = {}        
equipos = {}
partidos = []
try:
    with open("../data/historico_liga.txt", "r", encoding="cp1252") as archivo_partidos:
        for partido in archivo_partidos:
            temporada = codificar_edicion(partido[0:9].strip())
            division = partido[13].strip()
            jornada = partido[21:23].strip()
            equipo_local = codificar_equipo(partido[28:44].strip(), equipos)
            equipo_visitante = codificar_equipo(partido[45:61].strip(), equipos)
            goles_local = partido[61:63].strip()
            goles_visitante = partido[64:66].strip()
            partidos.append({"temporada": temporada, "division": division, "jornada": jornada, "equipo_local": equipo_local, "equipo_visitante": equipo_visitante, "goles_local": goles_local, "goles_visitante": goles_visitante})
except IOError as e:
    print('Operation failed: %s' % e.strerror)
    
try:
    with open("../data/inserts_partidos.sql", "w", encoding="cp1252") as archivo_inserts:
        archivo_inserts.write("BEGIN TRANSACTION; \n")
        for nombre_equipo, id_equipo in equipos.items():
            insert = "INSERT INTO EQUIPOS(eq_id, eq_nombre, eq_tipo) VALUES (%s, '%s', %s);\n" % (id_equipo, nombre_equipo, 1)
            archivo_inserts.write(insert)
        for partido in partidos:
            insert = "INSERT INTO games(season, division, round, team_home, team_away, goals_home, goals_away) VALUES (%s, %s, %s, %s, %s, %s, %s);\n" % (partido['temporada'], partido['division'], partido['jornada'], partido['equipo_local'], partido['equipo_visitante'], partido['goles_local'], partido['goles_visitante'])
            archivo_inserts.write(insert) 
        archivo_inserts.write("COMMIT; \n")           
                
except IOError as e:
    print('Operation failed: %s' % e.strerror)